import React from "react";
import "../styles/Button.css";

function Button(props) {
  function add() {
    props.setNumber(3);
  }

  return (
    <div>
      <button class="add-button" onClick={add}>
        +
      </button>
    </div>
  );
}

export default Button;
