import React, { useState } from "react";
import "../styles/Home.css";
import Button from "../components/Button";

function Home() {
  const [number, setNumber] = useState(1);

  return (
    <div>
      <div class="add-div">
        <text class="add-text">{number}</text>
        <Button setNumber={setNumber}></Button>
      </div>
    </div>
  );
}

export default Home;
